import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Contact} from '../entities/contact.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  contacts: any;

  constructor(private http: HttpClient) { }

  contactEndpoint:string = 'https://s3.amazonaws.com/technical-challenge/v3/contacts.json';

  getContacts(){

    this.contacts = this.http.get(this.contactEndpoint);
    return this.contacts;
  }


}
