import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../services/contact.service';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  contacts: any;

  constructor( private Service: ContactService ) {

    this.Service.getContacts().subscribe( data => {
      this.contacts = data;
      console.log(this.contacts);
    });
  }

  ngOnInit() {
  }

  getFavs(){
    var favContacts = this.contacts.filter(obj => {
      return obj.isFavorite == true;
    })

    return favContacts;
  }

  getOther(){
    var otherContacts = this.contacts.filter(obj => {
      return obj.isFavorite == false;
    })

    return otherContacts;
  }

}
