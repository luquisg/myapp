import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '../../services/contact.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {

  contact: any;
  id: any;

  constructor(private Service: ContactService, private Router: ActivatedRoute, private _location: Location)
  {
    this.Router.params.subscribe( data => {
      this.id = data.id;
    });

    this.Service.getContacts().subscribe( data => {
      data.filter(obj => {
        if (obj.id == this.id) {
          this.contact = obj;
          console.log(this.contact);
        }
      })
    });
  }

  ngOnInit()
  {

  }

  backClicked() {
    this._location.back();
  }
}
