import {Address} from './address.model';
import {Phone} from './phone.model';

export class Contact {
  address: Address;
  birthdate: string;
  companyName: string;
  emailAddress: string;
  id: number;
  isFavorite: boolean;
  largeImageURL: string;
  name: string;
  phone: Phone;
  smallImageURL: string;
}

