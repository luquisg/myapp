import { RouterModule, Routes } from '@angular/router';

// Componentes

import { ContactComponent } from './contacts/contact-detail/contact.component';
import { ContactsComponent } from './contacts/contact-list/contacts.component';

const appRoutes:  Routes = [
    { path: 'contacts', component: ContactsComponent },
    { path: 'contact/:id', component: ContactComponent },
    { path: '**', redirectTo: 'contacts' },
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes);
