import { Pipe } from '@angular/core';

@Pipe({
  name: 'phone'
})
export class PhonePipe{
  transform(val, args) {
    val = val.charAt(0) != 0 ? '0' + val : '' + val;
    let newStr = '';

    newStr = '(' + val.substr(1, 3) + ') ';


    return newStr+ val.substr(5);
  }
}
