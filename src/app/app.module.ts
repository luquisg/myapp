import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { APP_ROUTES } from './app-routing.module';
// import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { ContactsComponent } from './contacts/contact-list/contacts.component';
import { ContactComponent } from './contacts/contact-detail/contact.component';
import { ContactService } from './services/contact.service';

import { HttpClientModule } from '@angular/common/http';
// import { NavbarComponent } from './shared/navbar/navbar.component';
import { PhonePipe } from './pipes/phoneNumber.pipe';

// const appRoutes: Routes = [
//   { path: 'contact/:id', component: ContactComponent },
//   // { path: 'hero/:id',      component: HeroDetailComponent },
//   // {
//   //   path: 'heroes',
//   //   component: HeroListComponent,
//   //   data: { title: 'Heroes List' }
//   // },
//   // { path: '',
//   //   redirectTo: '/heroes',
//   //   pathMatch: 'full'
//   // },
//   // { path: '**', component: PageNotFoundComponent }
// ];

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    ContactComponent,
    PhonePipe,
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    HttpClientModule
  ],
  providers: [
    ContactService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
